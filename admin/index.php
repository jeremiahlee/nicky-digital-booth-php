<?php 
require_once('../lib/config.php');
require_once('../lib/db.php');
require_once('../lib-ext/phpSmug/phpSmug.php');

if (session_id() == "") { @session_start(); }
?>
<html> 
<head>
	<title>Nicky Digital Photo Booth Admin</title>
</head>
<body>
<?php
try {
	$f = new phpSmug(
		'APIKey='.$config['smugMugKey'], 
		'AppName=Nicky Digital Photo Booth/1.0 (http://nickydigital.com)', 
		'OAuthSecret='.$config['smugMugSecret']
	);
	
	// Check if there is an existing token	
	$settingsQuery = $pdo->prepare('
		SELECT 
			*
		FROM 
			settings
		WHERE
			setting = ?
		ORDER BY id DESC
		LIMIT 1
	');
	$settingsQuery->setFetchMode(PDO::FETCH_ASSOC);
	$settingsQuery->execute(array('smugMugSessionToken'));
	$settingsQueryResult = $settingsQuery->fetch();
	
	if (!empty($settingsQueryResult)) {
		$token = json_decode($settingsQueryResult['value'], TRUE);
		$f->setToken("id={$token['Token']['id']}", "Secret={$token['Token']['Secret']}");
	}
	
	// Get list of public albums
	$albums = $f->albums_get( 'Heavy=True' );
	// echo '<pre>'.print_r($albums, true).'</pre>';
?>
<h1>Select the album to upload to:</h1>
<form action="admin-savealbum.php" method="post">
	<select name="album">
<?php
	foreach ( $albums as $album ) {
		echo '<option value="'.$album['id'].'">'.$album['Title'].' ('.$album['id'].')</option>'."\n";
	}
?>
	</select>
	<input type="submit" value="Save" />
</form>

<?php
} catch ( Exception $e ) {
	echo "{$e->getMessage()} (Error Code: {$e->getCode()})";
?>
	<h1><a href="admin-login.php">Login to SmugMug</a></h1>
<?php
}
?>
</body>
</html>