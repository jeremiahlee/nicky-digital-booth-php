<?php

// Connect to the database
require_once('../lib/db.php');

// Get the complete file list
// Return the file list output and close the connection
$allPhotoQuery = $pdo->prepare('
	SELECT
		id,
		filename,
		upload_status,
		smugmug_url
	FROM 
		photos
	ORDER BY
		id DESC
');
$allPhotoQuery->setFetchMode(PDO::FETCH_ASSOC);
$allPhotoQuery->execute();
$allPhotoQueryResult = $allPhotoQuery->fetchAll();

header('Content-type: application/json');
echo json_encode($allPhotoQueryResult);