SETUP
=====

- On computer: download MAMP from http://mamp.info/
- Startup MAMP
- Configure MAMP to point to this directory (the one with this file in it)

- Open browser on computer, go to: http://localhost:8888/phpMyAdmin/
- Create new database: booth
- Click on "booth" database in left column
- Click on SQL tab at top
- Copy contents of sql/booth.sql and sql/config.sql into "Run SQL query/queries on database booth" field
- Hit Go (database is now setup)

- Open browser on computer, go to: http://localhost:8888/admin/
- Follow directions to sign in to SmugMug
- Select the SmugMug Album where photos should be uploaded and hit Save

- Photos to be uploaded must be saved to the /images directory of this application!

- On computer: Apple Menu > System Preferences > Sharing
- Change your computer name to "photobooth"

- On iPad connected to same WiFi as computer: go to http://photobooth.local:8888
- Add to Home Screen
- Launch the new icon (page will now be full screen)

Web page will refresh every 30 seconds to find new photos to upload.
If sharing buttons do not appear when you click on a photo to see the full size, it has not yet been uploaded.
You can add your own background image by replacing the assets/sample-background.png file.
You can configure the default Tweet message by editing the lib/config.php file.

LATER
=====
- SmugMug pages need to have the og: tags in their metadata so they're picked up properly.
- MAMP max execution time?
- saving should be an update instead of insert
- Only querying photos based on selected album? How would we setup a new booth?
- Specifying other directory on harddrive?


SMUGMUG
=======
username: NickyDigital 
Password: hudson123
email address: Nicky@nickydigital.com

http://nickydigital.smugmug.com/homepage/controlpanel.mg
http://wiki.smugmug.net/display/API/
PHP lib: https://github.com/lildude/phpSmug


FACEBOOK
========
app name: NickyDigital
key: FdE0kvR2X9vQA19AIic0R1kdEMDeZt2P
secret: 86ece19c2b6ee602f65853e85282213a 