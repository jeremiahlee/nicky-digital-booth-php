<?php
require_once('../lib/config.php');
require_once('../lib/db.php');
require_once('../lib-ext/phpSmug/phpSmug.php');

// Set default retry timer in seconds
$delay = 30;


// Find next photo to upload
// -------------------------

$nextPhotoQuery = $pdo->prepare('
	SELECT 
		id,
		filename,
		upload_status
	FROM 
		photos
	WHERE
		upload_status < 100
	ORDER BY
		id ASC
	LIMIT 1
');
$nextPhotoQuery->setFetchMode(PDO::FETCH_ASSOC);
$nextPhotoQuery->execute();
$nextPhoto = $nextPhotoQuery->fetch();

// Upload the photo, if there is one
// ---------------------------------
	
if (!empty($nextPhoto)) {
	$delay = 1;
	
	$updatePhotoStatusQuery = $pdo->prepare('
		UPDATE
			photos
		SET
			upload_status = ?,
			upload_response = ?,
			smugmug_url = ?
		WHERE
			id = ?
		LIMIT 1
	');
	
	// try to connect to SmugMug
	try {
		$f = new phpSmug(
			'APIKey='.$config['smugMugKey'], 
			'AppName=Nicky Digital Photo Booth/1.0 (http://nickydigital.com)', 
			'OAuthSecret='.$config['smugMugSecret']
		);
			
		$settingsQuery = $pdo->prepare('
			SELECT 
				*
			FROM 
				settings
			WHERE
				setting = ?
			ORDER BY id DESC
			LIMIT 1
		');
		$settingsQuery->setFetchMode(PDO::FETCH_ASSOC);
		$settingsQuery->execute(array('smugMugSessionToken'));
		$settingsQueryResult = $settingsQuery->fetch();
		$token = json_decode($settingsQueryResult['value'], TRUE);
		
		$f->setToken("id={$token['Token']['id']}", "Secret={$token['Token']['Secret']}");
		
		// Get the album id
		$settingsQuery->execute(array('album'));
		$settingsQueryResult = $settingsQuery->fetch();
		$albumId = json_decode($settingsQueryResult['value'], TRUE);
		
		if (empty($albumId)) {
			error_log('ERROR: No album id specified.');
			$delay = 60;
		}
		
		// Mark photo as started
		$updatePhotoStatusQuery->execute(array(1, '', '', $nextPhoto['id']));
		
		// Upload this photo	
		$uploadResult = $f->images_upload("AlbumID={$albumId}", "File=../images/{$nextPhoto['filename']}");
		
		// Mark photo as uploaded and store response array
		$updatePhotoStatusQuery->execute(array(100, json_encode($uploadResult), $uploadResult['URL'], $nextPhoto['id']));
		
		error_log("SUCCESS: {$nextPhoto['id']} successfully uploaded.");
		
	} catch ( Exception $e ) {
		error_log('ERROR: Upload failed due error related to SmugMug: ');
		error_log('| '.$e->getMessage().' (Error Code: '.$e->getCode().')');
		
		$delay = 60;
	}

} else {
	error_log('STATUS: No photos needing to be uploaded.');
}

// Done! Add any photos that have been added to the directory
// ----------------------------------------------------------

// Fetch file list
if ($handle = opendir('../images')) {
	
	// Prepare repeated search query
	$photoQuery = $pdo->prepare('
		SELECT 
			id
		FROM 
			photos
		WHERE
			filename = ?
		LIMIT 1
	');
	$photoQuery->setFetchMode(PDO::FETCH_ASSOC);
	
	// Prepare repeated insert query
	$insertQuery = $pdo->prepare('
		INSERT INTO photos
			(filename)
		VALUES
			(?)
	');
	
	// Identify new files and add them to the database
	while (false !== ($filename = readdir($handle))) {
		if ($filename != "." && $filename != "..") {
			// Check if file is in database
			$photoQuery->execute(array($filename));
			$photoQueryResult = $photoQuery->fetch();
			
			// Add to database
			if (empty($photoQueryResult) === true) {
				error_log("STATUS: Added {$filename} to photos table.");
				$insertQuery->execute(array($filename));
				
				$delay = 1;
			}
		}
	}
	
	closedir($handle);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="refresh" content="<?php echo $delay; ?>" />
	<title>Nicky Digital Photo Booth Uploader</title>
	<script>
		window.setTimeout(function(){window.location.href=window.location.href}, <?php echo $delay; ?> * 1000);
	</script>
</head>
<body>
	This frame uploads new photos to SmugMug as they are added to the watch directory.
</body>
</html>