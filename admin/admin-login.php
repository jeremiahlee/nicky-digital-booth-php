<?php 
require_once('../lib/config.php');
require_once('../lib/db.php');
require_once('../lib-ext/phpSmug/phpSmug.php');

if (session_id() == "") { @session_start(); }
?>
<html> 
<head>
	<title>Nicky Digital Photo Booth Admin Login</title>
</head>
<body>
<?php
try {
	$f = new phpSmug(
		'APIKey='.$config['smugMugKey'], 
		'AppName=Nicky Digital Photo Booth/1.0 (http://nickydigital.com)', 
		'OAuthSecret='.$config['smugMugSecret']
	);

	// Perform the 3 step OAuth Authorisation process.
	if ( ! isset( $_SESSION['SmugGalReqToken'] ) ) {
		// Step 1: Get a Request Token
		$d = $f->auth_getRequestToken();
		$_SESSION['SmugGalReqToken'] = serialize( $d );

		// Step 2: Get the User to login to SmugMug and Authorise this demo
		echo "<p>Click <a href='".$f->authorize('Access=Full', 'Permissions=Modify')."' target='_blank'><strong>here</strong></a> to login to SmugMug.</p>";
        echo "<p>Once you've signed in to SmugMug, <a href='".$_SERVER['PHP_SELF']."'>refresh this page</a>.</p>";
	} else {
		$reqToken = unserialize( $_SESSION['SmugGalReqToken'] );
		unset( $_SESSION['SmugGalReqToken'] );

		// Step 3: Use the Request token obtained in step 1 to get an access token
		$f->setToken("id={$reqToken['Token']['id']}", "Secret={$reqToken['Token']['Secret']}");
		$token = $f->auth_getAccessToken();	// The results of this call is what your application needs to store.
		
		// Store token in the database
		$insertQuery = $pdo->prepare('
			INSERT INTO 
			settings (
				setting,
				value,
				modified_at
			) 
			VALUES (
				?,
				?,
				NOW()
			)
		');
		$insertQuery->execute(array(
			'smugMugSessionToken',
			json_encode($token)
		));
		
		// Set the Access token for use by phpSmug.   
		$f->setToken( "id={$token['Token']['id']}", "Secret={$token['Token']['Secret']}" );
?>
<a href="/admin/index.php">Great! Go back.</a>
<?php
		
	}

} catch ( Exception $e ) {
	echo "{$e->getMessage()} (Error Code: {$e->getCode()})";
}
?>
</body>
</html>