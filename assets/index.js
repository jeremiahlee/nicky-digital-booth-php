// Photo Viewing
var bigphotoImg = document.getElementById("bigphoto");
var bigphotoInitalValue = bigphotoImg.src;

var sharephotoDiv = document.getElementById("sharephoto");

var urlToShare = "";

function showPhoto(e) {	
	// Set the URL that will get shared if the user chooses to share
	urlToShare = e.target.getAttribute("data-smugmug");
	
	// Display the share buttons if the image is sharable
	if (urlToShare) {
		document.getElementById("sharefacebook").className = "";
		document.getElementById("sharetwitter").className = "";
	} else {
		document.getElementById("sharefacebook").className = "hidden";
		document.getElementById("sharetwitter").className = "hidden";
	}
	
	// Display the big photo
	sharephotoDiv.className = "";
	
	// Set big image src
	bigphotoImg.setAttribute("src", e.target.src);
}

function closePhoto() {
	// Hide the big photo
	sharephotoDiv.className = "hidden";
	
	// Reset the big photo src to the loading animation
	bigphotoImg.setAttribute("src", bigphotoInitalValue);
}

document.getElementById("close").addEventListener("click", closePhoto);


// Photo Sharing
document.getElementById("sharefacebook").addEventListener("click", shareFacebook);
document.getElementById("sharetwitter").addEventListener("click", shareTwitter);

function shareFacebook() {
	var facebookShare = 
		'https://www.facebook.com/dialog/feed?app_id=356284597770313'
		+ '&link=AWESM_URL'
		+ '&redirect_uri='
		+ encodeURIComponent('http://photobooth.local:8888/')
		+ '&display=popup'
	;
	
	var facebookAuth = 
		'https://www.facebook.com/dialog/oauth?client_id=356284597770313'
		+ '&redirect_uri=' + encodeURIComponent(facebookShare) 
		+ '&auth_type=reauthenticate'
		+ '&display=popup'
	;
	
	var awesmShare = 
		'https://api.awe.sm/url/share?v=3'
		+ '&url=' + encodeURIComponent(urlToShare)
		+ '&key=3e7c9d37f5d9f9d1da5a2ba5b9245597fcdefc827544dbfff520ec70f9fe5224' // 5c8b1a212434c2153c2f2c2f2c765a36140add243bf6eae876345f8fd11045d9 // 3e7c9d37f5d9f9d1da5a2ba5b9245597fcdefc827544dbfff520ec70f9fe5224'
		+ '&tool=82oZef'
		+ '&channel=facebook-post'
		+ '&destination=' + encodeURIComponent(facebookAuth)
	;
	
	window.location = awesmShare;
	
}
function shareTwitter() {
	window.location = 
	'https://api.awe.sm/url/share?v=3'
	+ '&url=' + encodeURIComponent(urlToShare)
	+ '&key=3e7c9d37f5d9f9d1da5a2ba5b9245597fcdefc827544dbfff520ec70f9fe5224' // 3e7c9d37f5d9f9d1da5a2ba5b9245597fcdefc827544dbfff520ec70f9fe5224'
	+ '&tool=82oZef'
	+ '&channel=twitter'
	+ '&destination=' + encodeURIComponent(
		'https://twitter.com/intent/tweet?text='
		+ tweet 
		+ '&url=AWESM_URL&related=nickydigital'
	);
}


// Photo List
var photoListDiv = document.getElementById("photolist");

function refreshPhotoList() {
	// Query the photo list
	$.ajax({
		url: 'api/photos.php',
		success: renderPhotoList
	});
}

function renderPhotoList(photos) {
	var photoListHtml = '';
	
	for (var i = 0, iMax = photos.length; i < iMax; i++) {
		photoListHtml += '<img class="preview" id="photo-'
			+ photos[i].id
			+ '" src="images/'
			+ photos[i].filename
			+ '" data-smugmug="'
			+ ((photos[i].smugmug_url)? photos[i].smugmug_url : '')
			+ '" alt="" />';
	}
	
	photoListDiv.innerHTML = photoListHtml;
	
	// Attach click listeners
	for (i = 0; i < iMax; i++) {
		document.getElementById("photo-" + photos[i].id).addEventListener("click", showPhoto);
	}
}

// Initial render
refreshPhotoList();

// Set timer to continually reload photo list
// Every 30 seconds (30 * 1000)
var reloadTimer = window.setInterval(refreshPhotoList, 30000);