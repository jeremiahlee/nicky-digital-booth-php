<?php
// Connect to the database
try {
	$pdo = new PDO( 
		'mysql:host=localhost;dbname=booth', 
		'root', 
		'root', 
		array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") 
	);
} catch(PDOException $e) {
	die($e);
}