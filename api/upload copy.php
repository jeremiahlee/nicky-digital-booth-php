<?php
// Header handling for long running process
ob_end_clean();
header("Connection: close");
ignore_user_abort();
ob_start();

// Connect to the database
require_once('lib/db.php');


// Fetch file list
if ($handle = opendir('./images')) {
	
	// Prepare repeated search query
	$photoQuery = $pdo->prepare('
		SELECT 
			id
		FROM 
			photos
		WHERE
			filename = ?
		LIMIT 1
	');
	$photoQuery->setFetchMode(PDO::FETCH_ASSOC);
	
	// Prepare repeated insert query
	$insertQuery = $pdo->prepare('
		INSERT INTO photos
			(filename)
		VALUES
			(?)
	');
	
	// Identify new files and add them to the database
	while (false !== ($filename = readdir($handle))) {
		if ($filename != "." && $filename != "..") {
			// Check if file is in database
			$photoQuery->execute(array($filename));
			$photoQueryResult = $photoQuery->fetch();
			
			// Add to database
			if (empty($photoQueryResult) === true) {
				// echo $filename.': NO <br />';
				$insertQuery->execute(array($filename));
			} else {
				// echo $filename.': YES <br />';
			}
		}
	}
	
	closedir($handle);
}

// Get the complete file list
// Return the file list output and close the connection
$allPhotoQuery = $pdo->prepare('
	SELECT 
		filename,
		upload_status
	FROM 
		photos
	ORDER BY
		id DESC
');
$allPhotoQuery->setFetchMode(PDO::FETCH_ASSOC);
$allPhotoQuery->execute();
$allPhotoQueryResult = $allPhotoQuery->fetchAll();

// echo json_encode($allPhotoQueryResult);

// Close connection
$size = ob_get_length();
header('Content-type: application/json');
header("Content-Length: $size");
ob_end_flush();
flush();


// Find the new photos
$newPhotoQuery = $pdo->prepare('
	SELECT 
		id,
		filename,
		upload_status
	FROM 
		photos
	WHERE
		upload_status = 0
	ORDER BY
		id DESC
');
$newPhotoQuery->setFetchMode(PDO::FETCH_ASSOC);
$newPhotoQuery->execute();
$newPhotoQueryResult = $newPhotoQuery->fetchAll();

// Upload
$updatePhotoStatusQuery = $pdo->prepare('
	UPDATE
		photos
	SET
		upload_status = 1
	WHERE
		id = ?
	LIMIT 1
');

for ($i = 0; $i < count($newPhotoQueryResult); $i++) {
	// Upload photo
	$newPhotoQueryResult[$i]['filename'];
	
	// Mark it as uploaded in the database
	$updatePhotoStatusQuery->execute(array($newPhotoQueryResult[$i]['id']));
}




function uploadToSmugMug($filename) {
	// http://upload.smugmug.com/
}