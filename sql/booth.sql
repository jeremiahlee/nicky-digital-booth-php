CREATE TABLE `photos` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `filename` varchar(255) NOT NULL,
 `upload_status` int(11) NOT NULL DEFAULT '0',
 `upload_response` text,
 `smugmug_url` varchar(255) DEFAULT NULL,
 `created_at` datetime NOT NULL,
 `updated_at` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8