CREATE TABLE `settings` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `setting` varchar(255) NOT NULL,
 `value` text,
 `modified_at` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8