<?php
require_once('../lib/config.php');
require_once('../lib/db.php');

// Store token in the database
$insertQuery = $pdo->prepare('
	INSERT INTO 
	settings (
		setting,
		value,
		modified_at
	) 
	VALUES (
		?,
		?,
		NOW()
	)
');

$insertQuery->execute(array(
	'album',
	$_REQUEST['album']
));

echo '<h1>Saved! Album: '.$_REQUEST['album'].'</h1>';

//header("Location: /admin/index.php");
//exit();