<?php
	require_once('lib/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Nicky Digital Photo Booth</title>
	<link rel="stylesheet" type="text/css" href="assets/style.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<script src="assets/jquery-1.7.2.min.js"></script>
	<script>
		var tweet = "<?php echo $config['tweet'] ?>";
	</script>
</head>

<body>
	<div id="photolist"><em>Loading&hellip;</em></div>
	
	<div id="sharephoto" class="hidden">
		<img id="bigphoto" src="assets/loading.gif" alt="" />
		<div id="sharefacebook"></div>
		<div id="sharetwitter"></div>
		<div id="close"></div>
	</div>

	<iframe id="uploadframe" src="api/upload.php" width="1" height="1"></iframe>
	<script src="assets/index.js"></script>
</body>
</html>